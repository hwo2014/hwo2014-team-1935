var net = require("net");
var JSONStream = require('JSONStream');
var CarBot = require('./src/CarBot');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

var startTime = Date.now();
console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function () {
    // Quick race
    return send({
        msgType: "join",
        data: {
            name: botName,
            key: botKey
        }
    });

    // Specific race
    /*return send({
        "msgType": "createRace", "data": {
            "botId": {
                "name": botName,
                "key": botKey
            },
            "trackName": "germany",
            "password": "asdasd",
            "carCount": 1
        }
    });*/
});

function send(json) {
    client.write(JSON.stringify(json));
    return client.write('\n');
}

jsonStream = client.pipe(JSONStream.parse());

// Car bot
var russianDriver = new CarBot(send);

// Server message handling
jsonStream.on('data', function (data) {
    if (data.msgType === 'carPositions') {
        russianDriver.refresh(data.data);
    } else {
        switch (data.msgType) {
            case 'join':
                console.log('Joined');
                break;
            case 'createRace':
                console.log('Race created');
                break;
            case 'yourCar':
                var color = data.data.color;
                russianDriver.setCarColor(color);
                console.log('Your car is colored:', color);
                break;
            case 'gameStart':
                console.log('Race started');
                break;
            case 'gameEnd':
                var usedTime = (Date.now() - startTime) / 1000;
                console.log('Race ended. Runtime:', usedTime);
                break;
            case "gameInit":
                console.log("game init");
                russianDriver.setTrack(data.data.race.track);
                break;
            case "crash":
                console.log("awww shiiiiit");
                break;
            case 'spawn':
                console.log('Spawned');
                break;
            case 'turboAvailable':
                console.log('Got turbo!');
                russianDriver.setTurboToAvailable();
                break;
            case "lapFinished":
                console.log("LAAAAAP:", data.data.lapTime.millis / 1000);
                break;
            case 'finish':
                console.log('Car finished');
                break;
            case 'tournamentEnd':
                break;
            default :
                console.log('SUPER SECRET MESSAGE?!', data);
        }

        send({
            msgType: "ping",
            data: {}
        });
    }
});

jsonStream.on('error', function () {
    return console.log("disconnected");
});