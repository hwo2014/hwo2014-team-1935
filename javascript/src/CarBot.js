var _ = require('./libs/lodash.min');

/**
 * Car bot AI
 *
 * @param   {function}  send    Socket message sender.
 *                              Takes json string as parameter
 */
function CarBot (send) {
    var carColor;
    var previousPieceDistance = 0;
    var previousPieceIndex = 0;
    var currentSpeed = 0;
    var carInstructions;
    var gotTurbo = false;

    /**
     * Set car color for identification
     *
     * @param   {string}    color
     */
    this.setCarColor = function (color) {
        carColor = color;
    };

    /**
     * Creates car instructions from track data
     *
     * @param   {object}    track
     */
    this.setTrack = function (track) {
        carInstructions = analyzeTrack(track);
    };

    this.setTurboToAvailable = function () {
        gotTurbo = true;
    };

    /**
     * Determines the current speed of the car
     * @param   carPos  The car position
     */
    function determineCarSpeed(carPos) {
        if (carPos.pieceIndex == previousPieceIndex) {
            currentSpeed = carPos.inPieceDistance - previousPieceDistance;
        }
    }

    /**
     * Analyzes the track and makes initial assumptions for the throttle to use
     * @param track
     */
    function analyzeTrack(track) {
        var carInstructions = [];

        for (var i = 0; i < track.pieces.length ; i++) {
            var currentPiece = track.pieces[i];
            var instruction = { pieceIndex: i };
            var upcomingPieces = [];

            // Upcoming pieces
            for (var j = 1; j <= 2; j++) {
                if (track.pieces[i + j] !== undefined && track.pieces[i + j].angle !== undefined) {
                    upcomingPieces.push(track.pieces[i + j]);
                } else {
                    upcomingPieces.push(track.pieces[j - 1]);
                }
            }

            var analyzedUpcoming = analyzeUpcoming(upcomingPieces);

            // Determine throttle for track piece
            if (analyzedUpcoming.totalAngle == 0) {
                instruction.throttle = 1.0;
            } else {
                var angle = analyzedUpcoming.totalAngle;
                angle = (angle < 0) ? (angle * -1) : angle;

                // TODO needs some "real" calculations
                instruction.throttle = 1.0 - (angle / 165);
            }

            carInstructions.push(instruction);
        }

        return carInstructions;
    }

    /**
     * Determines the car's throttle based on the car's position
     *
     * @param carPos
     */
    function determineThrottle(carPos) {
        var pieceIndex = carPos.pieceIndex;
        return carInstructions[pieceIndex].throttle;
    }

    /**
     * Analyzes the given pieces for angles
     * @param pieces
     */
    function analyzeUpcoming(pieces) {
        var analyzed = {
            totalAngle: 0,
            totalRadius: 0,
            pieces: []
        };
        for (var i = 0; i < pieces.length ; i++) {
            analyzed.pieces.push(pieces[i]);
            if (pieces[i].angle !== undefined) {
                analyzed.totalAngle = analyzed.totalAngle + pieces[i].angle;
                analyzed.totalRadius = analyzed.totalRAdius + pieces[i].radius;
            }
        }

        return analyzed;
    }

    /**
     * Gets car position data by car color
     *
     * @param {object[]}    carPositions    Car positions data from server
     * @param {string}      color           Car color
     *
     * @returns {object}                    Position data for car
     */
    function getCarByColor(carPositions, color) {
        return _.find(carPositions, function (car) {
            return car.id.color == color;
        });
    }

    /**
     * Main driving function. Used on carPositions message
     *
     * @param   {object[]}    data    carPositions data
     */
    this.refresh = function (data) {
        // Our car
        var russianDriver = getCarByColor(data, carColor);
        var carpos = russianDriver.piecePosition;
        determineCarSpeed(carpos);
        var throttle = determineThrottle(carpos);
        /* TODO: Refactor this logic somewhere
         // Go through the lane, check if we're turning, and check that we're on the innermost lane
         var currentLaneIndex = carpos.lane.startLaneIndex;
         var currentTrack = track.lanes[currentLaneIndex];
         var switchDirection = "";
         for (var i = 0; i < track.lanes.length ; i++) {
         // Turn is positive -> turning right
         if (turnTotalAmount > 0 && track.lanes[i].distanceFromCenter > track.lanes[currentLaneIndex].distanceFromCenter) {
         currentTrack = track.lanes[i];
         switchDirection = "Right";
         } else if (turnTotalAmount < 0 && track.lanes[i].distanceFromCenter < track.lanes[currentLaneIndex].distanceFromCenter) { // Turn is negative -> turning left
         currentTrack = track.lanes[i];
         switchDirection = "Left";
         }
         }
         */

        // TODO use current speed and position when determining throttle

        //console.log(throttle);
        send({
            msgType: "throttle",
            data: throttle
        });

        previousPieceDistance = carpos.inPieceDistance;
        previousPieceIndex = carpos.pieceIndex;
    }
}

module.exports = CarBot;